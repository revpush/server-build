## версии
ansible 2.10.17

## волт
пароли и прочая чувствительная информация должна хранится в волте, в зашифрованом виде
### Создать симлинк
cd ./inventories/group_vars/all
ln -s ../../../vault.yml vault.yml
### Создать в корне pass файл, вписать в него пароль от волта
touch .vault_pass
### Работа с волт
чтобы добавить новую информацию в волт необходимо его открыть
ansible-vault decrypt vault.yml
внести изменения, сохранить и закрыть
ansible-vault encrypt vault.yml

## Настройка
### deploy-all-playbook.yml
указать группу хостов или хост   hosts: stage_php_servers
указать пользователя с правами sudo (пользователь должен мочь sudo без пароля)  remote_user: srg
### ./inventories/inventory.yml
добавить хосты, группы
### ./inventories/group_vars/*
создать папку с названием группы, как в инвентори файле ( например см. stage_php_servers )
внутри создать конфиги для сервисов ( например см. ../stage_php_servers/ )

## Запустить уставновку
ansible-playbook ./deploy-all-playbook.yml


